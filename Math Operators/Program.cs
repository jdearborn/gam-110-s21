﻿using System;

class Program
{
    static void Main(string[] args)
    {
        /* Write a C# program that reads two integers from standard input (the console).
          It should then prints out four separate values which are the result of each basic 
          math operation on them (e.g. User enters 2 and 4, programs prints out 2+4=6, 2-4=-2, 
          2*4=8, 2/4=0).

            Read two strings from standard input (reading from the console).

            Convert each string to an integer using int.Parse() or int.TryParse()

            Perform and print out the result of each basic math operation (+, -, *, /) between the two integers.
        */

        Console.WriteLine("Please type an integer.");
        string input1 = Console.ReadLine();

        //int value1 = int.Parse(input1);  // Throws an exception if the string does not contain an integer.
        // Exceptions are for "exceptional" errors and are a little trickier to handle.

        int value1;
        if(int.TryParse(input1, out value1))
        {
            // "Then" part of the conditional
            Console.WriteLine("Thanks!  That's an integer, alright.");
            Console.WriteLine("The value is " + value1);
        }
        else
        {
            // "Else" part of the conditional
            Console.WriteLine("Sorry, that's not an integer.");
            Console.ReadLine();
            return;
        }

        //Console.WriteLine("Addition: " + (value1 + value2));

        // This kind of division is "integer division", where it calculates without a decimal/remainder.
        //Console.WriteLine("Division: " + (12/4) + ", " + (7/4));

        // Get the remainder of integer division using the "modulo" operator:
        // int remainder = 7%4;  // Should be 3


        // Variables and literals
        float someDecimalNumber;
        someDecimalNumber = 0.5654f;
        someDecimalNumber = 27f;
        someDecimalNumber = 1.000001f;  // Stores up to around 7 digits of precision

        double someMorePreciseValue = 1.0000000000001;




        Console.ReadLine();
    }
}
