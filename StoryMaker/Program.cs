﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello!");

        Console.WriteLine("Please enter a Noun:");

        string noun1;
        noun1 = Console.ReadLine();

        Console.WriteLine("Please enter a Verb:");
        string verb1;
        verb1 = Console.ReadLine();

        Console.WriteLine();
        Console.WriteLine();

        Console.WriteLine("Thanks!  Now I will tell you a story...");
        Console.WriteLine();

        // String concatenation operator (+)
        Console.WriteLine("One day, a " + noun1 + " was walking down the road.");
        Console.WriteLine("Then, the " + noun1 + " started to " + verb1 + " and fell over.");

    }
}
