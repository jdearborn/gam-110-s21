﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        // Let's make a list of grades
        float[] grades = {90f, 50.5f, 100f, 85.5f, 71.4f, 65.4f, 92.3f, 88.8f};
        /*float[] grades = new float[25];
        for(int i = 0; i < grades.Length; ++i)
        {
            grades[i] = i;
        }*/

        // Indexing into the array starts at index 0
        Console.WriteLine("The first grade is: " + grades[0]);

        grades[0] += 2.5f;
        Console.WriteLine("The first grade after adjustment is: " + grades[0]);

        grades[0] = 90f;

        Console.WriteLine("Reset back to: " + grades[0]);


        // Print out all of the grades
        // for-loop parts: Initialization; looping test (condition); iteration (incrementing)
        for(int i = 0; i < grades.Length; ++i)
        {
            Console.Write(grades[i] + ", ");
        }
        Console.WriteLine();

        // How do we calculate an average?
        // Add all of the numbers up, then divide by the count of them.
        float sum = 0f;
        for(int i = 0; i < grades.Length; ++i)
        {
            sum += grades[i];
        }
        float average = sum / grades.Length;
        Console.WriteLine("Average: " + average);


        Console.ReadLine();
    }
}
