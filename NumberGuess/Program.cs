﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        Random rng = new Random();

        int answer = rng.Next(1, 101);

        Console.WriteLine("Hey, let's play a guessing game!");

        /*
                AND: True only if both sides are true
                OR: True if either input is true
        p   q   (p && q)    (p || q)    (p && (q || p))

        T   T       T           T           T
        F   F       F           F           F
        T   F       F           T           T
        F   T       F           T           F
        
         */

        while (true)
        {
            Console.WriteLine("Guess a number between 1 and 100.");
            string response = Console.ReadLine();

            int guess;
            //guess = int.Parse(response);
            if (int.TryParse(response, out guess))
            {
                if (guess == answer)
                {
                    Console.WriteLine("You win!");
                    break;
                }
                else if(guess < answer)
                {
                    Console.WriteLine("The answer is higher!");
                }
                else if(guess > answer)
                {
                    Console.WriteLine("The answer is lower!");
                }
            }
            else
            {
                Console.WriteLine("That's not a number!");
            }
        }

        Console.ReadLine();

    }
}
