﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        // Tic Tac Toe grid: 3x3, 9 values
        int[] array = new int[9];
        // string would work, but it's a bit overkill
        char[] grid = { '?', ' ', 'O',
                        'X', 'X', ' ',
                        ' ', ' ', '?'};

        // Printed as a linear array
        for(int i = 0; i < 9; ++i)
        {
            Console.WriteLine(grid[i]);
        }

        // Printed as a "fake" 2D array
        Console.WriteLine("------");
        for(int y = 0; y < 3; ++y)
        {
            for(int x = 0; x < 3; ++x)
            {
                Console.Write(grid[y*3 + x] + " ");
            }
            Console.WriteLine();
        }
        Console.WriteLine("------");


        //char[,] grid2D = new char[3,3];
        char[,] grid2D = {{'?', ' ', 'O' },
                          {'X', 'X', ' ' },
                          {' ', ' ', '?' }};

        // Printed as 2D array
        Console.WriteLine("------");
        for (int row = 0; row < 3; ++row)
        {
            for(int col = 0; col < 3; ++col)
            {
                Console.Write(grid2D[row,col] + " ");
            }
            Console.WriteLine();
        }
        Console.WriteLine("------");

        Console.WriteLine("Dimensions of 2D array: " + grid2D.GetLength(0) + " by " + grid2D.GetLength(1));



        // Fancy printing

        Console.WriteLine("-------------");
        for (int row = 0; row < 3; ++row)
        {
            Console.Write("|");
            for (int col = 0; col < 3; ++col)
            {
                Console.Write(" " + grid2D[row, col] + " |");
            }
            Console.WriteLine();
            Console.WriteLine("-------------");
        }

        Console.ReadLine();
    }
}
